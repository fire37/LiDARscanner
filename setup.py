from setuptools import setup

setup(name='AppOne',
version='1.0',
description='3D LiDAR Environment Scanner',
url='http://www.github.com/fire37/',
author='Julian Wiley',
author_email='julianwiley@yahoo.com',
license='Common',
packages=['sample'],
install_requires= [
'markdown',
'mpi4py',
'numpy',
'scipy',
'tkinter',
'paramiko'
],
zip_safe=false)
