import mpi4py
import numpy as np
import socket
import paramiko
import sys
import time
import logging
import logging.handlers as handlers

class RemoteWorker():
    def __init__(self, name):
        self.name = name
        self.worker_id = worker_id
        self.isConnected = isConnected
        self.ssh_ip = ssh_ip
        self.ssh_username = ssh_username
        self.ssh_password = ssh_password
        
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
        fh = logging.FileHandler('myApp.log')

    def connectSSH(self, target_id, target_username, target_password):
        # this will be compying the required files for the worker class and bootstrapping it to the
        # worker nodes

        SSH_ADDRESS = target_ip = self.ssh_ip
        SSH_USERNAME = target_username = self.ssh_username
        SSH_PASSWORD = target_password = self.ssh_password

        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        ssh_stdin = ssh_stdout = ssh_stderr = None

        try:
            ssh.connect(SSH_ADDRESS, username=SSH_USERNAME, password=SSH_PASSWORD)
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(SSH_COMMAND)
        except Exception as e:
            sys.stderr.write("SSH connection error: {0} ".format(e))

        if ssh_stdout:
            sys.stdout.write(ssh_stdout.read())
        if ssh_stderr:
            sys.stderr.write(ssh_stderr.read())

    def createWorker():
        # this will be compying the required files for the worker class and bootstrapping it to the
        # worker nodes

    def runScript(self, script_to_be_run):
