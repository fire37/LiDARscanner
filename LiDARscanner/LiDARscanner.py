# This code is meant to be deployed on a system that's connected to an arduino
import logging
import logging.handlers as handlers
import numpy as np
import sys
import threading
import csv
import serial
import sqlite3

class lidarScanner():

	def __init__(self):

	self.ser = serial.Serial('/dev/ttyUSB0',115200)

	#dataFile = open('data.csv', 'w', newline='')
	#dataReader = csv.reader(dataFile, csv.QUOTE_ALL)

	def turnRight(xStepsR):
		
		rStr = '1a'

		conn = sqlite3.connect('data.db')
		cur = conn.cursor()

		phi = []
		theta = [] 
		dist = []

		for i in range(xStepsR-1):

			self.ser.write(rStr.encode())
			tempPhi = ser.readline()
			tempTheta = ser.readline() 
			tempDist = ser.readline()

			phi.append(tempPhi)
			theta.append(tempTheta)
			dist.append(tempDist)

		for i in range(len(phi)):

			tempCommand = "INSERT INTO rawdata1 VALUES(%s, %s, %s);" % (phi[i], theta[i], dist[i])
    		cur.execute(tempCommand)
    		con.commit()

	def turnLeft(xStepsL):

		lStr = '1b'

		con = sqlite3.connect('data.db')
    	cur = con.cursor()

		stepStr = str(xStepsL) + 'b'

		self.ser.write(stepStr.encode())

		for i in range(0,xStepsL):

			tempPhi = ser.readline()
			tempTheta = ser.readline() 
			tempDist = ser.readline()

#		return(tempPhi, tempTheta, tempDist)

		tempCommand = "INSERT INTO rawdata1 VALUES(%s, %s, %s);" % (tempPhi, tempTheta, tempDist)
    	cur.execute(tempCommand)
    	con.commit()

	def tiltUp(yStepsU):

		uStr = '1c'

		con = sqlite3.connect('data.db')
    	cur = con.cursor()

		stepStr = str(yStepsU) + 'c'

		self.ser.write(stepStr.encode())

		for i in range(0,yStepsU):

			tempPhi = ser.readline()
			tempTheta = ser.readline() 
			tempDist = ser.readline()

		return(tempPhi, tempTheta, tempDist)

		tempCommand = "INSERT INTO rawdata1 VALUES(%s, %s, %s);" % (tempPhi, tempTheta, tempDist)
    	cur.execute(tempCommand)
    	con.commit()

	def tiltDown(yStepsD):

		dStr = '1d'

		con = sqlite3.connect('data.db')
    	cur = con.cursor()

		stepStr = str(yStepsD) + 'd'

		self.ser.write(stepStr.encode())

		for i in range(0,yStepsD):

			tempPhi = ser.readline()
			tempTheta = ser.readline() 
			tempDist = ser.readline()

		return(tempPhi, tempTheta, tempDist)

		tempCommand = "INSERT INTO rawdata1 VALUES(%s, %s, %s);" % (phi, theta, dist)
    	cur.execute(tempCommand)
    	con.commit()

	def run():
	        while True:

	                stepDir = input('Type right to turn to the right, left to turn to the left, up to tilt up, or down to tilt down.')
	                
	                stepd = input('How many steps to the right do you want to turn? integers only please.')
	                if stepDir == 'right':
	                        turnRight(stepd)
	                elif stepDir == 'left':
	                        turnLeft(stepd)
	                elif stepDir == 'up':
	                        tiltUp(stepd)
	                elif stepDir == 'down':
	                        tiltDown(stepd)
	                elif(stepDir == 'stop'):
	                        break