import threading
import LiDARscanner
import numpy as np
import sys
import time
import logging
from queue import Queue
from multiprocessing import Process, Queue, Pool 
import sqlite3

class Master():
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
        self.fh = logging.FileHandler
        self.connectedipAddresses = ['']
        self.scanner = LiDARscanner.lidarScanner()

    def turnRight(self, steps):
    	con = sqlite3.connect('data.db')
    	cur = con.cursor()

    	phi, theta, dist = self.scanner.turnRight(steps)

    	tempCommand = "INSERT INTO rawdata1 VALUES(%s, %s, %s);" % (phi, theta, dist)
    	cur.execute(tempCommand)
    	con.commit()

    def turnLeft(self, steps):
    	con = sqlite3.connect('data.db')
    	cur = con.cursor()

    	phi, theta, dist = self.scanner.turnLeft(steps)

    	tempCommand = "INSERT INTO rawdata1 VALUES(%s, %s, %s);" % (phi, theta, dist)
    	cur.execute(tempCommand)
    	con.commit()

    def tiltUp(self, steps):
    	con = sqlite3.connect('data.db')
    	cur = con.cursor()

    	phi, theta, dist = self.scanner.tiltUp(steps)

    	tempCommand = "INSERT INTO rawdata1 VALUES(%s, %s, %s);" % (phi, theta, dist)
    	cur.execute(tempCommand)
    	con.commit()

    def tiltDown(self, steps):
    	con = sqlite3.connect('data.db')
    	cur = con.cursor()

    	phi, theta, dist = self.scanner.tiltDown(steps)

    	tempCommand = "INSERT INTO rawdata1 VALUES(%s, %s, %s);" % (phi, theta, dist)
    	cur.execute(tempCommand)
    	con.commit()